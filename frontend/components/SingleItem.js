import React, { Component } from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Error from './ErrorMessage'
import styled from 'styled-components';
import HEAD from 'next/head'
import ItemStyles from "./styles/ItemStyles";
const SingleItemStyles = styled.div`
	max-width: 1200px;
	margin: 2rem auto;
	box-shadow : ${props => props.theme.bs};
	display : grid;
	grid-auto-columns : 1fr;
	grid-auto-flow:column;
	min-height: 800px;
	img {
		width : 100%;
		height : 100%;
		object-fit : cover;
	}
	.details {
		margin : 3rem;
		font-size : 2rem;
	}
`
const SINGLE_ITEM_QUERY = gql`
  query SINGLE_ITEM_QUERY($id: ID!) {
    item(where: { id: $id }) {
      id
      title
      description
      largeImage
		    image
    }
  }
`;
class SingleItem extends Component {
  render() {
    return (
      <Query query={SINGLE_ITEM_QUERY} variables={{ id: this.props.id }}>
        {({ error, loading, data }) => {
        	if (error) return <Error error={error}/>;
        	if(loading) return <p>Loading</p>;
        	if(!data.item) return <p>Not found element</p>;
        	const item = data.item;
        	console.log(item)
          return (
            <SingleItemStyles>
	            <HEAD>
		            <title>Sick fits | {item.title}</title>
	            </HEAD>
	            {item.image && <img src={item.image} alt={item.title}/>}
             <div className="details">
	             <h2>Viewing {item.title}</h2>
	             <p>{item.description}</p>
             </div>
            </SingleItemStyles>
          );
        }}
      </Query>
    );
  }
}

export default SingleItem;
